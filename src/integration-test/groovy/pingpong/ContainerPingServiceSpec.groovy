package pingpong

import com.groovycoder.spockdockerextension.Testcontainers
import com.rabbitmq.client.Channel
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.json.JsonOutput
import groovy.sql.Sql
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.integration.core.MessagingTemplate
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.GenericMessage
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.containers.wait.Wait
import spock.lang.Shared
import spock.lang.Specification

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageBuilder

/**
 * Created by jorge on 27/06/17.
 */
@Integration
@Rollback
@Testcontainers
class ContainerPingServiceSpec extends Specification {

    @Value('${pingpong.queue}')
    String queueName

    // tag::sharedRabbit[]
    @Shared
    GenericContainer rabbitmqContainer = new GenericContainer('rabbitmq:3')
            .withExposedPorts(5672)
            .waitingFor(Wait.forListeningPort())  // <1>
    // end::sharedRabbit[]

    // tag::sharedMysql[]
    @Shared
    MySQLContainer mysqlContainer = new MySQLContainer("mysql:5.5") //<2>
            .waitingFor(Wait.forListeningPort())
    // end::sharedMysql[]

    @Autowired
    SimpleMessageListenerContainer listenerContainer

    @Autowired
    AmqpTemplate amqpTemplate

    // tag::setupSpec[]
    def setupSpec() {
        String ip = rabbitmqContainer.containerIpAddress
        System.properties.setProperty("spring.rabbitmq.host",ip)      // <1>

        int mappedPort = rabbitmqContainer.getMappedPort(5672)
        System.properties.setProperty("spring.rabbitmq.port","$mappedPort")  // <2>

        System.properties.setProperty("spring.rabbitmq.username","guest")
        System.properties.setProperty("spring.rabbitmq.password","guest")

        println "rabbitmq configured $rabbitmqContainer.containerIpAddress"

        System.properties.setProperty("dataSource.url",mysqlContainer.jdbcUrl)      // <3>
        System.properties.setProperty("dataSource.username",mysqlContainer.username)      // <4>
        System.properties.setProperty("dataSource.password",mysqlContainer.password)
        System.properties.setProperty("dataSource.driverClassName", "com.mysql.jdbc.Driver")

        println Sql.newInstance(mysqlContainer.jdbcUrl, "root", "test", 'com.mysql.jdbc.Driver')
                .rows("select * from mysql.user").size()   //<5>


        println "mySQL configured $mysqlContainer.jdbcUrl"
    }
    // end::setupSpec[]

    def cleanup() {
        listenerContainer.stop()    //<1>
    }

    // tag::testPing[]
    void "test amqp ping"(){

        given: "mensaje"
        Message message = MessageBuilder.withBody("Hola".getBytes()).build()   // <1>

        when: "se envia por queue"
        Message response = amqpTemplate.sendAndReceive(queueName, message)   // <2>
        sleep(1000)

        then: "recibimos un pong"
        response
        response.body
        new String(response.body) == "Hola".reverse()     // <3>

        and: "lo hemos guardado"
        Sql.newInstance(mysqlContainer.jdbcUrl, "test", "test", 'com.mysql.jdbc.Driver')
                .rows("select * from message").size() == 1  //<4>

        and: "via Grails Domain"
        pingpong.Message.count() == 1        //<5>

    }
    // end::testPing[]

}
