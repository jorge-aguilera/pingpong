package pingpong

import com.groovycoder.spockdockerextension.Testcontainers
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.messaging.MessageChannel
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.Wait
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by jorge on 27/06/17.
 */
@Integration
@Rollback
class PingServiceSpec extends Specification {


    @Autowired
    PingService pingService

    def setupSpec() {
    }

    def setup(){
        println "Tenemos un ping service ? ${pingService!=null}"
    }

    void "test businnes ping"(){
        expect:
        pingService.newMessage("Hola") == "Hola".reverse()
    }

}
