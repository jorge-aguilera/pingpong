package pingpong

import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.annotation.Transformer
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.amqp.Amqp
import org.springframework.integration.dsl.support.GenericHandler
import org.springframework.integration.dsl.support.Transformers
import org.springframework.integration.handler.LoggingHandler
import org.springframework.integration.transformer.GenericTransformer

/**
 * Created by jorge on 27/06/17.
 */
@Configuration
class AmpqConfigurator {

    @Value('${pingpong.queue}')
    String queueName

    @Bean
    public SimpleMessageListenerContainer listenerContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.queueNames = [queueName]
        container;
    }

    @Bean
    Queue pingpongQueueBean(){
        new Queue(queueName, true,false,false)
    }

    @Bean
    IntegrationFlow pingFlow(SimpleMessageListenerContainer listenerContainer){
        IntegrationFlows.from(Amqp.inboundGateway(listenerContainer))
                .log(LoggingHandler.Level.INFO )
                .transform('new String(payload)')
                .handle('pingService','newMessage')
                .get()
    }

}
