package pingpong

import grails.transaction.Transactional

class PingService {

    @Transactional
    String newMessage( String msg ) {
        log.info "ping service received $msg "

        Message m = new Message(message: msg)
        if(m.validate())
            m.save()

        "$msg".reverse()
    }
}
